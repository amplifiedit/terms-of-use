**Amplified** **Labs**

**General Terms of Use**

**_Effective Date: June 20, 2016_**

Welcome to Amplified Labs!  Amplified Labs is the product division of Amplified IT, LLC (collectively referred to as "**Amplified**," "**we**," "**our**," or "**us**"). We’re delighted to have you visit our website and begin using the our growing suite of tools for the Google Apps for Education platform that re-imagine and improve the workflows, systems, and processes to make K-12 schools work for kids (the "**Tools**").

**PLEASE READ THIS AGREEMENT CAREFULLY BEFORE USING THE TOOLS**

By accessing or using our Tools, you signify that you have read, understood, and agree to be bound by these General Terms of Use (the "**Agreement**") and to the collection and use of your information as set forth in the [Amplified IT Privacy Policy](http://labs.amplifiedit.com/privacy). You’re allowed to use our Tools only if you can form a binding contract with Amplified, and only in compliance with this Agreement and all applicable local, state, national, and international laws, rules and regulations.

If you are accepting this Agreement on behalf of a customer, you represent and warrant that: (1) you have full legal authority to bind your employer, or the applicable entity, to these terms and conditions; (2) you have read and understand this Agreement; and (3) you agree, on behalf of the party you represent, to the terms of this Agreement. If you do not have the legal capacity to enter into this Agreement or the authority to bind your employer, you are not permitted to enter into this Agreement or use the Tools.

We may change our Tools and the terms in this Agreement at any time, with or without notice. If you are a School using our Tools, we will notify you via email in advance of any material changes to the terms of this Agreement. Upon making changes, we will update the "Effective Date" found at the top of this page. Your continued use of our Tools after any changes constitutes your acceptance of the modified Agreement. If you do not agree to the terms of this Agreement or any modified Agreement, then you are not permitted to use the Tools.  You may view the full change history of these terms by visiting [this linked repository.](https://bitbucket.org/amplifiedit/terms-of-use)

**USE OF OUR TOOLS**

1. **Eligible Users.** This Agreement applies to all individuals, schools, school districts, and related entities and organizations that sign up to use one or more of our Tools, including but not limited to administrators who access the Tools on their behalf (each a "**School**"), as well as all non-School visitors, users, and others, including teachers, students and their parents, who use the Tools (such individuals and Schools, collectively, "**Users**" or "**you**").

2. **Parental Consent.** Schools acknowledge and agree that they are solely responsible for compliance with the Children’s Online Privacy Protection Act of 1998 ("**COPPA**") and Family Educational Rights and Privacy Act ("**FERPA**"), including, but not limited to, obtaining parental consent concerning the collection of students’ personal information used in connection with the provisioning and use of the Tools or access to such information through the use of Third-Party Products (as defined herein). Schools are solely responsible for obtaining parental consent before allowing any User under the age of 13 to use any of the Tools.

3. **User** **Content.** You are fully responsible for your interactions with other Users, any Content you provide and your activity on the Tools, including the transmission, accuracy and completeness of your Content. Do not take any action or post anything that may expose Amplified or its Users to any harm or liability of any type. "**Content**" includes communications, materials, information, data, graphs, visualizations, opinions, photos, profiles, messages, notes, website links, text information, music, videos, designs, graphics, sounds, and any other content that you and/or other Users post or otherwise make available on or through the Tools.

We have no liability for your interactions with any other Users, or for any of your actions, inactions, your Content or the Content of other Users. We may review Content to determine whether it is illegal or violates our policies, and we may remove or refuse to display Content that we reasonably believe violates our policies or the law. But that does not necessarily mean that we review Content, so please don’t assume that we do.

4. **Updates.** We may also from time to time, as we see fit, develop and provide updates for the Tools. This may include upgrades, modifications, bug fixes, patches and other error corrections and/or new features (collectively, "**Updates**"). Certain Tools may not properly operate if you do not install all Updates. The Tools may automatically electronically upgrade the versions used on your computer system or other device. You expressly consent to such automatic Updates. Further, you agree that this Agreement (and any additional modifications of the same) will apply to any and all Updates to the Tools. We have no obligation to provide any Updates or to continue to provide or enable any particular features or functionality of any Tools.

5. **Monitoring and Suspension.** We may change, suspend, or terminate your use of the Tools at any time: (1) if we, in our sole discretion, determine that you are or have been in violation of this Agreement; (2) if we, in our sole discretion, determine that you have created risk or possible legal exposure for Amplified or any other User; (3) in response to requests by law enforcement or other government agencies; or (4) due to unexpected technical issues or problems. We will endeavor to notify you by email or at the next time you attempt to access the Tools after any such deactivation, termination or suspension.

6. **Consent to Use of Data.** You agree that we may collect and use technical data and related information, including but not limited to technical information about your computer or mobile device, operating system and software, peripherals, replies, comments, information, data, scripts, executable files, graphics, geo-data, and any contact information provided in connection with your use of the Tools and and any intellectual property therein, that is gathered periodically to facilitate the provision of support and other services to you (if any) relating to the Tools or used to create Updates. We may use this information, as long as it is in a form that does not personally identify you, to improve our products or to provide services or technologies to you.

**PRIVACY AND SECURITY**

We take the collection, use and security of the personal data that our Users provide us very seriously. Our Privacy Policy, which is incorporated into this Agreement by this reference, further describes the collection, security and use of information.

**SUBSCRIPTION-BASED TOOLS**

1. **Subscription-Based Tools.** We offer ongoing access to and use of a full feature set for all of our Tools and dedicated email support for an annual subscription fee (a "**Subscription**"). A Subscription may be based on usage by an individual User or for the User’s domain. The Subscription will be subject to the terms of this Agreement. By signing up for a Subscription, you agree to pay the subscription fee and any other charges that may be incurred. You are entitled to access only those premium or non-trial features of the Tools for which you have a Subscription.

2. **Payment Terms.** When you sign up for a Subscription, you must designate and provide information about your preferred payment method ("**Payment Method**"). This information must be complete and accurate, and you are responsible for keeping it up to date. You expressly authorize us to collect from your Payment Method the appropriate fees charged for the Subscription which are paid on an annual basis. All fees due for the Subscriptions are payable in advance, and will be billed automatically to the Payment Method at the start of the annual subscription period. Unless otherwise stated, Subscriptions will auto-renew until you elect to cancel your access to the Tool. All Subscription fees are final and non-refundable, except at our sole discretion.

3. **Termination or Cancellation**. If you do not pay the Subscription fee or fail to comply with any other term of this Agreement, we may make reasonable efforts to notify you and resolve the issue; however, we reserve the right to disable or terminate your access to the Tool immediately without prior notice.

You can cancel the Subscription at any time by providing written notice to us. The cancellation of a Subscription will go into effect at the end of your current annual billing cycle, and you will have the same level of access to the Tool through the remainder of such billing cycle. For example, if you cancel prior to the end of the annual period, you will be charged for the entirety of the year and maintain access to the Tool through the end of the annual period.

There are no refunds for termination or cancellation of your Subscription. If you no longer wish to subscribe, it is your responsibility to cancel your Subscription in due time, regardless of whether or not you actively use the Tool.

4. **Fee Changes.** To the maximum extent permitted by applicable laws, we may change our Subscription prices at any time. We will give you reasonable notice of any such pricing changes by posting the new prices on or through the applicable Tool and/or by sending you an email notification. If you do not wish to pay the new prices at your next renewal, you can cancel the applicable Subscription in accordance with the terms of this Agreement.

5. **Free Trials.** We may offer free trials of our Tools that have limited features or functionality for immediate installation by Google Account users ("**Free Trial**").  A Free Trial provides you with access to the Tools for a period of time and is subject to the terms of this Agreement and as well as any additional details specified when you sign up for the offer. We may, without prior notice, change, cancel, create usage limits for, or permanently or temporarily stop offering or allowing you access to the Free Trial.

6. **Tool Changes.** We continually update our Tools to offer the best possible suite of products. We may make changes to the features or functions of Tools at any time, and we cannot guarantee that any specific feature or function will be available for the entire Subscription period.  If you are not satisfied with any changes you are encouraged to contact our support team.

7. **Notice of Changes.** While we will try to give you advance notice of changes that will adversely affect you, this might not be practical or possible and we retain the right to make Changes without advance notice.

8. **Installation.** Our Tools may be standalone Web applications, or they may be installed by individual Google Account users from one or more Google application marketplaces, such as the Chrome Web Store or the Android Play Store.  Depending on whether or not you are using a Google Apps for Education account, and also on the settings employed on your Google Apps domain, you may or may not be able to utilize these application marketplaces to install one of our Tools on your own individual Google Account.  If you are a Google domain super-administrator, you may be able to use the Google Apps Marketplace to install our Tools on behalf of some or all of the Users on your Google Apps domain. Do not install a Tool on behalf of a Google Apps domain or an organizational unit on your domain if you do not have the requisite authority to do so.

9. **Google Account Limitations.** Google imposes both [published quotas](https://developers.google.com/apps-script/guides/services/quotas) and unpublished spam protections on its services, and these services can have outages that can directly impact the performance of our Tools.  While we attempt to design our Tools to assist in identifying the source of a limitation, the performance of the Tools cannot exceed that of the underlying Google services on which they depend.

10. **Safeguard Your Username/Password**. You are responsible for any activity within our Tools that occurs from your Google Account, or in the case of Google domain-level Tool installation, for any activity that occurs in our Tools by Users on your Google Apps domain.  Because our Tools are typically accessible under the authority of the User’s Google Account login, we recommend using or forcing Users to use "strong" passwords (passwords that use a combination of upper and lower case letters, numbers, and symbols) and/or Google’s two-factor authentication with your account to avoid unauthorized use. Please reset your Google account credentials, or contact your Google domain administrator immediately if you think your account’s security has been compromised. We are not liable for any losses of any kind caused by any unauthorized use of your Google Account in the use of our Tools.

**NOTICES**

By providing your email address in the subscription process, you consent to our using your email address to send notices related to the Tools, including any required legal notices and other messages, such as additions or changes to the Tools and/or notices of product offers. You may opt out or change your preferences by using the opt out instructions indicated in the footer of our mailings. While we need your email address to send you important notices regarding the Tools, you can always ask us to stop sending you certain marketing messages. Opting out may prevent you from receiving email messages regarding updates, improvements, or offers.

**INTELLECTUAL PROPERTY**

1. **Ownership.** Our Tools and all materials made available to you in connection with them, including, but not limited to, software, documentation, source and object code, frameworks, workflows, techniques, procedures, images, text, graphics, illustrations, logos, photographs, audio, videos, music, usage analytics data, hosting infrastructure for the Tools, and any copies, modifications or derivative works thereof, in whole or in part, and all related Intellectual Property Rights, are and will remain the sole and exclusive property of Amplified and its licensors (the "**Amplified Property**"). Except as specified in this Agreement, nothing herein may be deemed to create a license or convey any Intellectual Property Rights in the Amplified Property to any User.

2. **Limited License to Use.** Subject to the terms and conditions of this Agreement, we grant you a non-exclusive, non-transferable, non-sublicenseable, revocable limited license to use our Tools, in object code form only, solely (i) on a device that you own or control, and (ii) for your personal, non-commercial, or educational purposes. The Tools are licensed to you, not sold, under this Agreement. We reserve all rights not expressly granted to you in the Tools. We may terminate this license at any time for any or no reason.

3. **Restrictions on Your Use.** Please don’t do anything to harm our products or try to hack our Tools. Specifically, by using the Tools, you agree not to, and not to allow any third party to, do any of the following things:

    1. to sell, license, rent, modify, distribute, copy, reproduce, transmit, publicly display, publicly perform, publish, adapt, edit or create derivative works from any Amplified Property;

    2. identify or discover any source code of the Tools;

    3. change or delete any ownership notices from the Tools or any related documentation;

    4. copy, distribute, sell, sublicense or disclose any part of the Tools or any documentation in any medium, including but not limited to by any automated or non-automated "scraping";

    5. use any automated system, including but not limited to "robots," "spiders," "offline readers," etc., to access the Tools in a manner that sends more request messages to our servers than a human can reasonably produce in the same period of time by using a conventional online Web browser;

    6. transmit spam, chain letters, or other unsolicited email;

    7. attempt to interfere with, compromise the system integrity or security or decipher any transmissions to or from the servers running the Tools;

    8. take any action that imposes, or may impose an unreasonable or disproportionately large load on our infrastructure;

    9. upload invalid data, viruses, worms, or other software agents through the Tools;

    10. collect or harvest any third-party personally identifiable information, including account names or Student Data (as defined below) from the Tools;

    11. use our Tools for any commercial solicitation purposes;

    12. impersonate another person or otherwise misrepresenting your affiliation with a person or entity, conducting fraud, hiding or attempting to hide your identity;

    13. interfere with the proper working of the Tools;

    14. access any content on the Tools through any technology or means other than those provided or authorized by the Tools; or

    15. bypass the measures we use to prevent or restrict access to the Tools, including but not limited to features that prevent or restrict use or copying of any content or enforce limitations on use of the Tools or related content.

4. **Feedback.** If you choose to send us ideas, feedback or comments ("**Feedback**"), you agree that we may use the Feedback you send without any restriction or compensation to you. By receiving your Feedback, Amplified does not waive any rights to use similar or related ideas previously known to us, developed by our employees, or obtained from sources other than you. You certify and represent that Feedback you send to us is not confidential or proprietary information.

5. **Trademarks**. Amplified owns or licenses all Amplified trademarks, service marks, branding, logos and other similar assets (the "**Trademarks**"). You are not authorized to copy, imitate, modify, display or otherwise use the Trademarks (in whole or in part) for purposes other than in connection with the use of the Tools, without our prior written approval.



6. "**Student Data**" is any information (in any format) that is directly related to any identifiable current or former student that is maintained by a School, and may include "educational records" as defined by the FERPA. While our Tools may need to access, or be employed by you to handle Student Data as part of providing their function, except as required by a defined scope of work inside a school support engagement, we do not take possession of or responsibility for Student Data which is owned and controlled at all times by the School. Our [Privacy Policy](https://docs.google.com/document/d/1gUgF2cGh3YsqlK-Sm6DgQu98RpvSwhyGHTbF-wVFI9M/edit) provides more detail around how we handle Student Data.

7. "**Intellectual Property Rights**" means all worldwide patent rights, copyright rights, moral rights, rights of publicity, trademark, trade dress and service mark rights, goodwill, trade secret rights, and other intellectual property rights as may now exist or hereafter come into existence, and all related applications and registrations, renewals and extensions.

**DATA RETENTION**

Certain data may be stored by or on behalf of Amplified on our servers in connection with your use of our Tools.  This consists primarily of anonymous analytical data or information relating to your use of the Tools, but may also include data or information that Users such as students and teachers have stored on our servers in the process of using our Tools ("**Data**"). It is our expectation that Data will primarily be stored in a User’s Google Account and is governed by the terms of use and data privacy policies of Google, Inc.

Amplified will retain your Data, if any, for a period of thirty (30) days following termination of this Agreement (the "**Retention Period**"). Amplified has no obligation to retain any Data beyond the Retention Period for any User or School. You understand and acknowledge that following expiration of the Retention Period, Amplified has the right to delete your Data from its servers in its sole discretion.

It is your responsibility to keep copies of your Data in other secure storage.  Schools have the sole responsibility to ensure copies of Student Data are stored on their student information system or other secure storage.  Amplified will not be responsible for any loss of Data.

**THIRD PARTY SERVICES**

Our website and Tools may include links to websites, applications, services, products or materials of other companies that are not owned or controlled by us ("**Third-Party Products**"). We do not endorse, warrant, guarantee, monitor or have any control over these Third-Party Products, which have separate terms of use and privacy policies. Amplified is not responsible for the content or policies of any Third-Party Products. You understand that you access Third-Party Products at your own risk and agree that we have no associated liability.

**CONFIDENTIAL INFORMATION**

You agree to hold in the strictest confidence and not disclose to third parties or use, except to the extent necessary to perform any obligations under this Agreement, Amplified IT’s proprietary or confidential information including technical, business, financial or other information or trade secrets, whether or not specifically marked as confidential, or information which would be deemed by a reasonable person to be confidential or proprietary in nature ("**Confidential Information**").  Confidential Information shall not include information that (a) is in or enters the public domain without breach of this Agreement, (b) is received from a third party who is entitled to disclose such information without restriction on disclosure and without breach of a nondisclosure obligation, (c) you knew prior to receiving such information or develops independently without reference to the Confidential Information, or (d) is disclosed pursuant to a judicial or other governmental order, provided that you shall give us reasonable notice prior to such disclosure and comply with any applicable protective order or equivalent.  You agree that the rights being protected by this Section are of a special and unique character, which gives them a particular value, and that the breach of this Section could result in irreparable injury and damage to Amplified. In such event, we may be entitled to require specific performance, obtain injunctive and other equitable relief in any court of competent jurisdiction to prevent the violation or threatened violation of this Section.

**INDEMNIFICATION**

To the maximum extent permitted by applicable law, you agree to defend, indemnify and hold harmless Amplified, its subsidiaries and affiliates, and their respective agents, licensors, managers, employees, contractors, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to reasonable attorney’s fees and court costs) arising from or relating in any way to: (i) your use of and access to the Tools, including any data or Content transmitted or received by you; (ii) your violation of any term of this Agreement; (iii) your violation of any third-party rights, including but not limited to any right of privacy or Intellectual Property Rights; (iv) your violation of any applicable law, rule or regulation, including but not limited to COPPA and FERPA; (v) any Content or information that is submitted via your account; or (vi) any other party’s access and use of the Tools with your unique username, password or other appropriate security code. We reserve the right, at your expense, to assume the exclusive defense and control of any matter for which you are required to indemnify us under this Agreement, and you agree to cooperate with our defense of these claims.

**DISCLAIMER OF WARRANTIES**

THE TOOLS ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS WITH ALL FAULTS AND WITHOUT WARRANTIES OF ANY KIND. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, AMPLIFIED EXPRESSLY DISCLAIMS ALL WARRANTIES, REPRESENTATIONS AND GUARANTEES OF ANY KIND, WHETHER ORAL OR WRITTEN, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. WITHOUT LIMITING THE FOREGOING, AMPLIFIED, ITS SUBSIDIARIES, ITS AFFILIATES, AND ITS LICENSORS DO NOT WARRANT THAT (I) THE TOOLS WILL FUNCTION ACCURATELY, RELIABLY OR CORRECTLY; (II) THE TOOLS WILL MEET YOUR REQUIREMENTS OR YOU WILL ACHIEVE ANY SPECIFIC RESULTS; (III) THE TOOLS WILL BE AVAILABLE AT ANY PARTICULAR TIME OR LOCATION, UNINTERRUPTED, ERROR-FREE OR SECURE; (IV) ANY DEFECTS OR ERRORS WILL BE CORRECTED; (V) THE TOOLS ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS; OR (VI) ANY USER CONTENT IS CORRECT OR ACCURATE. YOU UNDERSTAND AND EXPRESSLY AGREE YOUR USE OF THE TOOLS AND ANY CONTENT DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE TOOLS IS AT YOUR OWN RISK AND YOU WILL BE SOLELY RESPONSIBLE FOR YOUR USE OF THE TOOLS AND ANY DAMAGE TO YOUR COMPUTER SYSTEM OR ANY OTHER DEVICE IN WHICH YOU ACCESS THE TOOLS, LOSS OF DATA OR OTHER HARM OF ANY KIND THAT MAY RESULT.

FEDERAL LAW, SOME STATES, PROVINCES, AND OTHER JURISDICTIONS DO NOT ALLOW EXCLUSIONS AND LIMITATIONS OF CERTAIN IMPLIED WARRANTIES, SO SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU.

**LIMITATION OF LIABILITY**

TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL AMPLIFIED, ITS AFFILIATES, AGENTS, DIRECTORS, OFFICERS, EMPLOYEES, SUPPLIERS, OR LICENSORS BE LIABLE FOR ANY INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL, OR EXEMPLARY DAMAGES, INCLUDING WITHOUT LIMITATION DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA, REVENUE, OR OTHER INTANGIBLE LOSSES, THAT RESULT FROM (I) THE USE OF, OR INABILITY TO USE, THE TOOLS, (II) THE CONDUCT OF OTHER USERS OF THE TOOLS (WHETHER ONLINE OR OFFLINE), (III) ANY USER-GENERATED CONTENT, OR (IV) ANY THIRD-PARTY PRODUCTS ACCESSED VIA THE TOOLS. UNDER NO CIRCUMSTANCES WILL AMPLIFIED BE RESPONSIBLE FOR ANY DAMAGE, LOSS OR INJURY RESULTING FROM HACKING, TAMPERING, OR OTHER UNAUTHORIZED ACCESS OR USE OF THE TOOLS OR YOUR ACCOUNT OR THE INFORMATION CONTAINED THEREIN.

TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, AMPLIFIED ASSUMES NO LIABILITY OR RESPONSIBILITY FOR ANY (I) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT; (II) PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO OR USE OF OUR TOOLS; (III) ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION STORED THEREIN; (IV) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM THE TOOLS; (V) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE THAT MAY BE TRANSMITTED TO OR THROUGH OUR TOOLS BY ANY THIRD PARTY; (VI) ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE INCURRED AS A RESULT OF THE USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE THROUGH THE TOOLS; AND/OR (VII) SCHOOL CONTENT OR THE DEFAMATORY, OFFENSIVE, OR ILLEGAL CONDUCT OF ANY THIRD PARTY. IN NO EVENT SHALL AMPLIFIED, ITS AFFILIATES, AGENTS, DIRECTORS, OFFICERS, EMPLOYEES, SUPPLIERS, OR LICENSORS BE LIABLE TO YOU FOR ANY CLAIMS, PROCEEDINGS, LIABILITIES, OBLIGATIONS, DAMAGES, LOSSES OR COSTS IN AN AMOUNT EXCEEDING THE AMOUNT YOU PAID TO AMPLIFIED HEREUNDER OR $100.00, WHICHEVER IS GREATER.

THIS LIMITATION OF LIABILITY SECTION APPLIES WHETHER THE ALLEGED LIABILITY IS BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY, OR ANY OTHER BASIS, EVEN IF AMPLIFIED HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE, AND FURTHER WHERE A REMEDY SET FORTH HEREIN IS FOUND TO HAVE FAILED ITS ESSENTIAL PURPOSE. SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATIONS OR EXCLUSIONS MAY NOT APPLY TO YOU. THIS AGREEMENT GIVES YOU SPECIFIC LEGAL RIGHTS, AND YOU MAY ALSO HAVE OTHER RIGHTS WHICH VARY FROM STATE TO STATE. THE DISCLAIMERS, EXCLUSIONS, AND LIMITATIONS OF LIABILITY UNDER THIS AGREEMENT WILL NOT APPLY TO THE EXTENT PROHIBITED BY APPLICABLE LAW.

**COMPLIANCE WITH LAWS**

While Amplified’s servers are located inside the United States, our Tools are controlled and operated in some cases from Google’s server facilities, which may or may not be located inside the United States, and we make no representations that they’re appropriate or available for use in other locations. If you access or use the Tools from other jurisdictions, you understand that you’re entirely responsible for compliance with all applicable United States and local laws and regulations, including but not limited to export and import regulations. You may not use the Tools if you are a resident of a country embargoed by the United States, or are a foreign person or entity blocked or denied by the United States government. You will indemnify and hold Amplified harmless from any and all claims, losses, liabilities, damages, fines, penalties, costs and expenses (including, but not limited to, attorney’s fees) arising from or relating to any breach by you of your obligations under this Section.

**TERM AND** **SURVIVAL**

This Agreement and the license granted hereunder are effective on the date you first download the Tools and shall continue unless this Agreement is terminated as set forth herein. Upon termination of this Agreement, the license granted hereunder will terminate and you must stop all use of the Tools. You will continue to be bound by the terms of this Agreement that by their nature may survive termination including, without limitation, Intellectual Property, Indemnification, Disclaimer of Warranties, Limitation of Liabilities and Governing Law.

**MODIFICATIONS TO THIS AGREEMENT**

** **

Amplified reserves the right to modify this Agreement by posting the modified Agreement on our website. The modified version of this Agreement will become effective as of the date posted on the website unless otherwise specified in the modified Agreement. Your use of the Tools following that date constitutes your acceptance of the terms and conditions of the Agreement as modified. If you do not agree to the modifications, you are not permitted to use, and should discontinue your use of, the Tools. Modifications will not apply retroactively unless required by law.

**GENERAL LEGAL TERMS**

1. **Governing Law; Venue. **You agree that: (i) the Tools will be deemed solely based in the Commonwealth of Virginia; and (ii) the Tools will be deemed passive services that do not give rise to personal jurisdiction over us, either specific or general, in jurisdictions other than the Commonwealth of Virginia. This Agreement will be governed by the laws of the Commonwealth of Virginia, without respect to its conflict of laws principles. The application of the United Nations Convention on Contracts for the International Sale of Goods does not apply. You agree to submit to the personal jurisdiction of the federal and state courts located in Norfolk, Virginia, for any actions related to this Agreement.

2. **Assignment**. This Agreement, and any rights and licenses granted hereunder, may not be transferred or assigned by you, but may be assigned by us in accordance with the terms of our Privacy Policy.

3. **Entire Agreement.** This Agreement, together with any amendments, the Privacy Policy and any additional agreements you may enter into with Amplified in connection with the Tools including other agreements referenced herein, constitute the entire agreement between you and Amplified concerning the Tools.

4. **Severability.** If any provision of this Agreement is deemed invalid by a court of competent jurisdiction, the invalidity of such provision will not affect the validity of the remaining provisions of this Agreement, which will remain in full force and effect.

5. **No Continuing Waiver.** No waiver of any term of this Agreement will be deemed a further or continuing waiver of such term or any other term, and Amplified’s failure to assert any right or provision under this Agreement will not constitute a waiver of such right or provision.

6. **Commercial Items.** If access to the Tools is licensed to the United States government or any agency thereof, then the Tools will be deemed to be "commercial computer software" and "commercial computer software documentation," pursuant to as such terms are used in 48 C.F.R. §12.212 or 48 C.F.R. §227.7202, as applicable. Consistent with 48 C.F.R. 12.212 and 48 C.F.R. 227.7202-1 through 227.7202-4 (JUNE 1995), the Tools and all related documentation are provided to U.S. Government End Users only as a commercial items and with only those rights as are granted to all other customers pursuant to the terms and conditions herein.

**HOW TO CONTACT US**

If you have any feedback, questions or comments about the Tools, please contact our support team by email at [labs@amplifiedit.com](mailto:labs@amplifiedit.com), and include the subject as "Attn: Legal – General Terms of Use."

** **

**Thanks for reading all of this and welcome to Amplified LABS!**

